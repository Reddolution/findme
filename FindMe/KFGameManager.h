//
//  KFGameManager.h
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

typedef enum : NSUInteger {
    KFGameModeNormal = 1,
    KFGameModeEndless = 2,
    KFGameModeQuick = 3,
    KFGameModeTutorial = 4,
} KFGameMode;

typedef enum : NSUInteger {
    RDSoundTap = 0,
    RDSoundCorrect = 1,
    RDSoundError = 2,
    RDSoundStartGame = 3
} RDSound;

@interface KFGameManager : NSObject {
    int currEndless;
    KFGameMode gameMode;
    NSMutableArray* gameSounds;
}

@property NSArray* master;




+ (KFGameManager*) sharedInstance;

+ (NSArray*) setUpGame;
+ (NSMutableArray*) genNums;

+ (int) optimalTaps;


+ (KFGameMode) gameMode;
+ (void) setGameMode: (KFGameMode) gameMode;

+ (void) playAudio : (RDSound) sound;

+ (void) sendGameOverStuff : (int) taps : (int) time : (int) wrongTaps : (NSArray*) master : (NSMutableArray*) endless : (int) nextCount : (int) score;

@end

static KFGameManager* shared;