//
//  ViewController.m
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "ViewController.h"
#import "GameViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //See if I want to play gamecenter
        // Do any additional setup after loading the view, typically from a nib.
    //Streaks!!!
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"dayStreak"] == nil) { //Loading for everything
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"totalTaps"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"totalTime"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"totalGamesPlayed"];
        
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"dayStreakLongest"];
        [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"dayStreak"];
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:[NSDate date]];
        NSDate *startDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        [[NSUserDefaults standardUserDefaults] setObject:startDate forKey:@"dayDate"];
    } else {
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:[NSDate date]];
        NSDate *startDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        NSDate * pastDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"dayDate"];
        
        int current = [[[NSUserDefaults standardUserDefaults] objectForKey:@"dayStreak"] intValue];
        
        if([startDate timeIntervalSinceDate:pastDate] > 24*60*60) {
            [[NSUserDefaults standardUserDefaults] setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"dayStreak"] forKey:@"dayStreakLongest"];
            if(current > 1) {
                if(current > 20) {
                    [Flurry logEvent:@"STREAK_ENDED_21andOver" withParameters:[NSDictionary dictionaryWithObject:[NSString stringWithFormat:@"%i",current] forKey:@"days"]];
                } else if(current> 10) {
                    [Flurry logEvent:@"STREAK_ENDED_11-20"];
                } else if(current > 5) {
                    [Flurry logEvent:@"STREAK_ENDED_6-10"];
                } else {
                    [Flurry logEvent:@"STREAK_ENDED_2-5"];
                }
            }
            [[NSUserDefaults standardUserDefaults] setObject:@"0" forKey:@"dayStreak"];
            [[NSUserDefaults standardUserDefaults] setObject:startDate forKey:@"dayDate"];
            current = 0;
        }
        if(current > 0) {
            UILabel* label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
            [label setFont:[UIFont fontWithName:@"BebasNeueBold" size:17.0f]];
            [label setBackgroundColor:[UIColor colorWithRed:0.436 green:0.711 blue:0.900 alpha:1.000]];
            [label setTextAlignment:NSTextAlignmentCenter];
            label.layer.cornerRadius = 4;
            [label setText:[NSString stringWithFormat:@"%i day streak!",current]];
            [self.view addSubview:label];
            dayStreak = label;
        }
    }
}
- (void)loadView {
    [super loadView];

}
- (void)viewWillAppear:(BOOL)animated {
    [Appodeal showAd:AppodealShowStyleBannerBottom rootViewController:self];
    
//    [Appodeal showAd:AppodealShowStyleInterstitial rootViewController:self];
}
- (void)viewDidAppear:(BOOL)animated {
    
    int current = [[[NSUserDefaults standardUserDefaults] objectForKey:@"dayStreak"] intValue];
    if(current > 0) {
        if(dayStreak != nil) {
            [dayStreak setText:[NSString stringWithFormat:@"%i day streak!",current]];
        }
        
    }
    if([[DataSender dictObjectForKey:@"FirstTime"] boolValue]) {
        //Showuppopup viewcontroller that asks if they want to use gamecenter
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Would you like to use GameCenter?" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"BebasNeueRegular" size:24.0f], NSParagraphStyleAttributeName : paragraphStyle}];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        
       
        CNPPopupButton *buttonYES = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
        CNPPopupButton *buttonNO = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
        
        CNPPopupController* popup = [[CNPPopupController alloc] initWithContents:@[titleLabel,buttonYES,buttonNO]];
        [buttonYES setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonYES.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [buttonYES setTitle:@"YES" forState:UIControlStateNormal];
        buttonYES.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
        buttonYES.layer.cornerRadius = 4;
        buttonYES.selectionHandler = ^(CNPPopupButton *button){
            [popup dismissPopupControllerAnimated:YES];
            [Flurry logEvent:@"ENABLED_GAMECENTER"];
            [DataSender dictSetObject:@"NO" forKey:@"FirstTime"];
            [DataSender dictSetObject:@"YES" forKey:@"GameCenter"];
            [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
            [self performSegueWithIdentifier:@"playTutorial" sender:self];
            
        };
        [buttonNO setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonNO.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [buttonNO setTitle:@"NO" forState:UIControlStateNormal];
        buttonNO.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
        buttonNO.layer.cornerRadius = 4;
        buttonNO.selectionHandler = ^(CNPPopupButton *button){
            [popup dismissPopupControllerAnimated:YES];
            [DataSender dictSetObject:@"NO" forKey:@"FirstTime"];
            [DataSender dictSetObject:@"NO" forKey:@"GameCenter"];
            [self performSegueWithIdentifier:@"playTutorial" sender:self];
        };
        
        popup.theme.popupStyle = CNPPopupStyleCentered;
        popup.delegate = self;
        [popup presentPopupControllerAnimated:YES];
    }
//    GADRequest *request = [GADRequest request];
//    request.testDevices = @[@"f84f0b1fd5937b3aef6b355efdced557"];
//    [Appodeal showAd:AppodealShowStyleBannerTop rootViewController:self];
//    [Appodeal setBannerDelegate:self];
//    [Appodeal isReadyForShowWithStyle:AppodealShowStyleBannerTop];
    
}
- (void)bannerDidFailToLoadAd {
    NSLog(@"NOOO");
}
- (void)popupControllerDidDismiss:(CNPPopupController *)controller {
    
    if([[DataSender dictObjectForKey:@"FirstTime"] boolValue]) {
        [DataSender dictSetObject:@"NO" forKey:@"FirstTime"];
        [DataSender dictSetObject:@"YES" forKey:@"GameCenter"];
        [self performSegueWithIdentifier:@"playTutorial" sender:self];
    }
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([[segue identifier ]isEqualToString:@"playNormal"]) {
        [KFGameManager setGameMode:KFGameModeNormal];
        GameViewController* vc = [segue destinationViewController];
        [vc loadStage];
    } else if ([[segue identifier ]isEqualToString:@"playEndless"]){
        [KFGameManager setGameMode:KFGameModeEndless];
        GameViewController* vc = [segue destinationViewController];
        [vc loadStage];
    } else if ([[segue identifier] isEqualToString:@"playQuick"]) {
        [KFGameManager setGameMode:KFGameModeQuick];
        GameViewController* vc = [segue destinationViewController];
        [vc loadStage];
    } else if ([[segue identifier] isEqualToString:@"playTutorial"]) {
        [KFGameManager setGameMode:KFGameModeTutorial];
        GameViewController* vc = [segue destinationViewController];
        [vc loadStage];
    }
}

- (IBAction)openGameCenter:(id)sender {
    if(![[DataSender dictObjectForKey:@"GameCenter"] boolValue]) {
        //Showuppopup viewcontroller that asks if they want to use gamecenter
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"You have GameCenter disabled. Would you like to use GameCenter?" attributes:@{NSFontAttributeName : [UIFont fontWithName:@"BebasNeueRegular" size:24.0f], NSParagraphStyleAttributeName : paragraphStyle}];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        
        
        CNPPopupButton *buttonYES = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
        CNPPopupButton *buttonNO = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 260, 60)];
        
        CNPPopupController* popup = [[CNPPopupController alloc] initWithContents:@[titleLabel,buttonYES,buttonNO]];
        [buttonYES setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonYES.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [buttonYES setTitle:@"YES" forState:UIControlStateNormal];
        buttonYES.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
        buttonYES.layer.cornerRadius = 4;
        buttonYES.selectionHandler = ^(CNPPopupButton *button){
            [popup dismissPopupControllerAnimated:YES];
            [DataSender dictSetObject:@"YES" forKey:@"GameCenter"];
            [[GameKitHelper sharedGameKitHelper] authenticateLocalPlayer];
            
        };
        [buttonNO setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        buttonNO.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [buttonNO setTitle:@"NO" forState:UIControlStateNormal];
        buttonNO.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
        buttonNO.layer.cornerRadius = 4;
        buttonNO.selectionHandler = ^(CNPPopupButton *button){
            [popup dismissPopupControllerAnimated:YES];
            [DataSender dictSetObject:@"NO" forKey:@"GameCenter"];
        };
        
        popup.theme.popupStyle = CNPPopupStyleCentered;
        popup.delegate = self;
        [popup presentPopupControllerAnimated:YES];
    }
    else {
        GKGameCenterViewController* leaderboardController = [[GKGameCenterViewController alloc] init];
        
        if (leaderboardController != NULL)
        {
            leaderboardController.viewState = GKGameCenterViewControllerStateLeaderboards;
            leaderboardController.gameCenterDelegate = self;
            [self presentViewController:leaderboardController animated:YES completion:nil];
        }
    }
}

#pragma mark gamecenter
-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)unwindToViewController:(UIStoryboardSegue *)segue {
    //nothing goes here
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) showLicense {
    [self performSegueWithIdentifier:@"credits" sender:self];
}


@end
