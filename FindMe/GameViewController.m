//
//  GameViewController.m
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "GameViewController.h"
#import "JHChainableAnimations.h"
#import "CircleProgressBar.h"
#import "GameOverViewController.h"
#define kColorThatBlueColor [UIColor colorWithRed:0.400 green:0.634 blue:0.986 alpha:1.000]

@interface GameViewController () {
    NSArray *colors;
}

@end

@implementation GameViewController

- (IBAction)unwindToGameViewController:(UIStoryboardSegue *)segue
{
    // Restart game
    [self loadStage];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    colors = @[[UIColor colorWithRed:0.8 green:0.4 blue:0.4 alpha:1.0],
               [UIColor colorWithRed:0.3 green:0.8 blue:0.3 alpha:1.0],
               [UIColor colorWithRed:0.4 green:0.4 blue:0.9 alpha:1.0]];
    
    maxWidth = self.view.bounds.size.width;
    maxHeight = self.view.bounds.size.height;
    if (maxHeight > 800) {
        device = DeviceiPad;
    } else {
        device = DeviceiPhone;
    }
    fontSize = 25.0f;
    if (device == DeviceiPad) {
        fontSize = 50.0f;
    }
    
    //Values
//    [self.label_tapCount setBackgroundColor:[UIColor colorWithWhite:0.5 alpha:1.0]];
//    self.label_tapCount.layer.cornerRadius = 10.0f;
    [self.label_tapCount setAlpha:0];
    [self.label_timeCount setAlpha:0];
    // Middle
    
    backButton = [[KFTile alloc] initWithFrame:[self makeCGRect:10]];
    backButton.value = @"BACK";
    [backButton setUpTile:fontSize * 2 - 20];
    [backButton addTarget:self action:@selector(backButton) forControlEvents:UIControlEventTouchUpInside];
    
    [backButton addTarget:self action:@selector(makeMePretty:) forControlEvents:UIControlEventTouchDown];
    [backButton addTarget:self action:@selector(makeMeUgly:) forControlEvents:UIControlEventTouchUpInside];
    [backButton addTarget:self action:@selector(makeMeUgly:) forControlEvents:UIControlEventTouchUpOutside];
    // Right
    
    barTime = [[CircleProgressBar alloc] initWithFrame:[self makeCGRect:11]];
    
    [barTime setProgressBarWidth:15];
    [barTime setHintViewSpacing:6];
    if (device == DeviceiPad) {
        [barTime setProgressBarWidth:10 * 2.5];
        [barTime setHintViewSpacing:5 * 2.5];
    }
    
    [barTime setHintTextFont:[UIFont fontWithName:@"BebasNeueRegular" size:fontSize]];
    [barTime setStartAngle:-90];
    [barTime setBackgroundColor:[UIColor clearColor]];
    [barTime setProgress:1 animated:NO];
    [barTime setProgressBarProgressColor: [UIColor colorWithRed:0.502 green:0.938 blue:1.000 alpha:1.000]];
    [barTime setProgressBarTrackColor:[UIColor colorWithWhite:0.827 alpha:1.000]];
    
    GameViewController *vs = self;
    [barTime setHintTextGenerationBlock:^NSString *(CGFloat progress) {
        return [NSString stringWithFormat:@"%i", vs->timeCount];
    }];
    
    menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, maxWidth, ((device == DeviceiPad) ? 42 : 25))];
    [menuButton setBackgroundColor: [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0]];
    [menuButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
    [menuButton.titleLabel setFont:[UIFont fontWithName:@"BebasNeueRegular" size:fontSize - 5]];
    [menuButton addTarget:self action:@selector(menuSelected:) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setTitleEdgeInsets:UIEdgeInsetsMake(4.0f, 0.0f, 0.0f, 0.0f)];
    
    menuButtonCancel = [[UIButton alloc] initWithFrame:CGRectMake(0, ((device == DeviceiPad) ? 53 : 38), maxWidth, maxHeight - 38)];
    [menuButtonCancel setBackgroundColor:[UIColor colorWithWhite:1.0 alpha:0.3]];
    [menuButtonCancel addTarget:self action:@selector(menuSelected:) forControlEvents:UIControlEventTouchUpInside];
    
    hint = [[UILabel alloc] initWithFrame:CGRectMake(15.f, -50.f, maxWidth - 30, ((device == DeviceiPad) ? 65 : 40))];
    hint.font = [UIFont fontWithName:@"BebasNeueBold" size:fontSize];
    hint.textAlignment = NSTextAlignmentCenter;
    hint.textColor = [UIColor blackColor];
    hint.backgroundColor =
    [UIColor colorWithRed:0.790 green:0.166 blue:0.166 alpha:1.000];
    hint.exclusiveTouch = false;
    hint.layer.cornerRadius = maxWidth / 35;
    [hint setClipsToBounds:YES];
    
    CNPPopupButton *buttonOKAY =
    [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, (device == DeviceiPad)?500:260, 60)];
    tutorialText = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, (device == DeviceiPad)?500:260,(device == DeviceiPad)?200: 120)];
    tutorialText.numberOfLines = 8;
    tutorialText.textAlignment = NSTextAlignmentCenter;
    tutorialText.lineBreakMode = NSLineBreakByWordWrapping;
    [tutorialText setFont:[UIFont fontWithName:@"BebasNeueRegular" size:fontSize]];
    
    tutorialController = [[CNPPopupController alloc] initWithContents:@[ tutorialText, buttonOKAY ]];
    if(device == DeviceiPad)
        tutorialController.theme.maxPopupWidth = 528;
    [buttonOKAY setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    buttonOKAY.titleLabel.font =
    [UIFont fontWithName:@"BebasNeueBold" size:fontSize];
    [buttonOKAY setTitle:@"Okay" forState:UIControlStateNormal];
    buttonOKAY.backgroundColor =
    [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    buttonOKAY.layer.cornerRadius = 4;
    buttonOKAY.selectionHandler = ^(CNPPopupButton *button) {
        [tutorialController dismissPopupControllerAnimated:YES];
    };
    
    
    tutorialController.theme.popupStyle = CNPPopupStyleCentered;
    tutorialController.delegate = self;
    
    [self.view addSubview:barTime];
    [self.view addSubview:backButton];
    [self.view addSubview:menuButton];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(addPenalty)
     name:UIApplicationWillResignActiveNotification
     object:nil];
    
    
}
- (void)popupControllerWillDismiss:(CNPPopupController *)controller
{
    gameState &= (GamePlaying | GamePrepare | GameEnded | GameViewTime);
    if (tutorialStep == 7) {
        [self performSegueWithIdentifier:@"rewindHome" sender:nil];
    }
}
- (void)addPenalty
{
    if (gameState == GameViewTime) {
        timeCount = 0;
        return;
    }
    if ([KFGameManager gameMode] == KFGameModeNormal ||
        [KFGameManager gameMode] == KFGameModeQuick) {
        timeCount += 15;
    } else if ([KFGameManager gameMode] == KFGameModeEndless) {
        timeCount -= 10;
    }
}

- (void)menuSelected:(UIView *)sender
{
    [menuButton.layer removeAllAnimations];
    [menuButtonCancel.layer removeAllAnimations];
    if (sender == menuButton) {
        // Either confirm or first tap
        if (menuButtonCancel.superview == nil) { // First tap
            menuButtonCancel.alpha = 0;
            [self.view addSubview:menuButtonCancel];
            menuButtonCancel.makeOpacity(1.0).animate(0.2);
            menuButton.anchorTop.makeHeight(((device == DeviceiPad) ? 53 : 38))
            .bounce.animate(0.3);
            [menuButton setTitle:@"Do you want to exit?" forState:UIControlStateNormal];
        } else { // Confirmed
            [self performSegueWithIdentifier:@"rewindHome" sender:self];
        }
    } else { // That means its the 'cancel' view
        menuButtonCancel.makeOpacity(0).animateWithCompletion(0.2, JHAnimationCompletion() {
            [menuButtonCancel removeFromSuperview];
        });
        
        menuButton.anchorTop.makeHeight(((device == DeviceiPad) ? 42 : 25))
        .bounce.animate(0.3);
        [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
    }
}

- (void)tutorialTapped
{
    gameState |= GamePaused;
    for (Pulser *p in tutorialPulses) {
        [self pulseClean:p];
    }
    
    [tutorialPulses removeAllObjects];
    
    switch (tutorialStep) {
        case 0:
            tutorialText.text = @"The first 16 seconds is your view time. Use view time to look through the folders and memorize numbers!";
            [tutorialController presentPopupControllerAnimated:YES];
        {
            // Backbutton
            Pulser *pulse = [[Pulser alloc] init];
            pulse.parent = backButton;
            pulse.oldColor = pulse.parent.backgroundColor;
            [tutorialPulses addObject:pulse];
            
            // Random folder
            pulse = [[Pulser alloc] init];
            int random = 0;
            pulse.parent = [master.children objectAtIndex:random];
            pulse.oldColor = pulse.parent.backgroundColor;
            tutorialFolder = pulse.parent;
            [tutorialPulses addObject:pulse];
        }
            break;
        case 1:
            tutorialText.text = @"When view time is over, the game begins.\nFind and "
            @"tap the numbers from 1 to 36!";
            [tutorialController presentPopupControllerAnimated:YES];
            [self findValue:nextRequiredTap];
            break;
        case 2:
        case 3:
        case 4:
        case 5: {
            if (tutorialStep % 2 == 0) {
                Pulser *pulse = [[Pulser alloc] init];
                pulse.parent = backButton;
                pulse.oldColor = pulse.parent.backgroundColor;
                [tutorialPulses addObject:pulse];
                tutorialFolder = backButton;
            } else {
                [self findValue:nextRequiredTap];
            }
        } break;
        case 6:
            tutorialText.text = @"That's it! We hope you enjoy :)";
            [tutorialController presentPopupControllerAnimated:YES];
            break;
        default:
            break;
    }
    
    
    tutorialStep++;
}


- (void)pulseAll
{
    UIColor *a = [UIColor colorWithRed:0.960 green:1.000 blue:0.572 alpha:1.000];
    UIColor *b = [UIColor colorWithRed:0.590 green:1.000 blue:0.480 alpha:1.000];
    for (Pulser *pulse in tutorialPulses) {
        if ([pulse.parent.backgroundColor isEqual:a])
            [pulse.parent setBackgroundColor:b];
        else
            [pulse.parent setBackgroundColor:a];
    }
}

- (void)pulseClean:(Pulser *)pulse
{
    pulse.parent.backgroundColor = pulse.oldColor;
}

- (void)findValue:(int)value
{
    for (int i = 0; i < [master.children count]; i++) {
        KFTile *currFolder = [master.children objectAtIndex:i];
        
        for (int j = 0; j < [currFolder.children count]; j++) {
            KFTile *currValue = [currFolder.children objectAtIndex:j];
            int val = [currValue.value intValue];
            
            if (val == value) {
                Pulser *nextNum = [[Pulser alloc] init];
                nextNum.parent = currValue;
                nextNum.oldColor = nextNum.parent.backgroundColor;
                [tutorialPulses addObject:nextNum];
                tutorialFolder = currFolder;
                Pulser *folder = [[Pulser alloc] init];
                folder.parent = currFolder;
                folder.oldColor = nextNum.parent.backgroundColor;
                [tutorialPulses addObject:folder];
                
                return;
            }
        }
    }
}

- (void)loadStage
{
    gameState = GamePrepare;
    
    [barTime setProgressBarProgressColor:
     [UIColor colorWithRed:0.502 green:0.938 blue:1.000 alpha:1.000]];
    [barTime setProgressBarTrackColor:[UIColor colorWithWhite:0.827 alpha:1.000]];
    
    NSArray *stageData = [KFGameManager setUpGame];
    
    data_master = stageData;
    data_endless = [[NSMutableArray alloc] init];
    
    master = [self loadStageWithData:stageData];
    tapCount = 0; // One for the first and last
    tapWrongCount = 0;
    nextRequiredTap = 1;
    consecWrong = 0;
    if ([KFGameManager gameMode] == KFGameModeQuick) {
        timeCount = 6;
    } else {
        timeCount = 16;
    }
    
    gameState = GameViewTime;
    [self selectTile:master];
    [menuButton setBackgroundColor: [UIColor colorWithRed:0.502 green:0.938 blue:1.000 alpha:1.000]];
    gameTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(userIsLooking:) userInfo:nil repeats:YES];
    
    if ([KFGameManager gameMode] == KFGameModeTutorial) {
        tutorialPulses = [[NSMutableArray alloc] init];
        tutorialTimer = [NSTimer scheduledTimerWithTimeInterval:0.6 target:self selector:@selector(pulseAll) userInfo:nil repeats:YES];
        
        [self tutorialTapped];
    } else {
        hint.text = @"Your view time begins!";
        hint.tag = [self.view ap_makeToastView:hint duration:3.f position:APToastPositionBottom];
    }
}

- (void)userIsLooking:(NSTimer *)timer
{
    if (gameState == GameViewTime) {
        timeCount -= 1;
        [barTime setProgress:((timeCount) / ([KFGameManager gameMode] == KFGameModeQuick ? 6.0 : 16.0))animated:YES];
        if (timeCount < 0) {
            [self selectTile:master];
            gameState = GamePlaying;
            if ([KFGameManager gameMode] == KFGameModeTutorial) {
                
                [self tutorialTapped];
            } else {
                hint.text = @"The Game Begins!";
                [self.view ap_ejectToast:hint.tag];
                hint.tag = [self.view ap_makeToastView:hint duration:3.f position:APToastPositionBottom];
            }
            
            [KFGameManager playAudio:RDSoundStartGame];
            [barTime setProgressBarProgressColor:[colors objectAtIndex:0]];
            [menuButton setBackgroundColor:
             [UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0]];
            
            if ([KFGameManager gameMode] == KFGameModeEndless) {
                timeCount = 40;
            } else {
                timeCount = 0;
            }
        }
        [_label_timeCount
         setText:[NSString stringWithFormat:@"Free View: %i", timeCount]];
    }
    
    if (gameState == GamePlaying) {
        consecWrong++;
        if ([KFGameManager gameMode] == KFGameModeEndless) {
            timeCount -= 1;
            if (timeCount < 0) {
                gameState = GameEnded;
            }
            
        } else {
            timeCount += 1;
        }
        
        if (timeCount % 60 == 0) {
            // New color
            int rand = arc4random() % [colors count];
            while ([[colors objectAtIndex:rand]
                    isEqual:[barTime progressBarProgressColor]]) {
                rand = arc4random() % [colors count];
            }
            [barTime setProgressBarTrackColor:[barTime progressBarProgressColor]];
            [barTime setProgressBarProgressColor:[colors objectAtIndex:rand]];
            [barTime setProgress:0 animated:NO];
        }
        
        [barTime setProgress:((timeCount % 60) / 60.0)animated:YES];
        
        [_label_timeCount
         setText:[NSString stringWithFormat:@"Time: %i", timeCount]];
    }
    
    if (gameState == GameEnded) {
        if ([KFGameManager gameMode] == KFGameModeTutorial) {
            
            [self performSegueWithIdentifier:@"rewindHome" sender:self];
        } else if ([KFGameManager gameMode] == KFGameModeNormal) {
            int optimal = [KFGameManager optimalTaps];
            float a = (tapCount - optimal) * 8;
            float b = (timeCount - (optimal)*1.5) * 5;
            
            _score = 1000 - (int)(a + b + tapWrongCount * 20);
            if (_score < 1) {
                _score = 1;
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreNormal"] == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreNormal"];
            } else if (_score > [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreNormal"] integerValue]) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreNormal"];
            }
            
        } else if ([KFGameManager gameMode] == KFGameModeEndless) {
            if (nextRequiredTap == 1) {
                _score = 0;
            } else {
                _score = nextRequiredTap * 10 - 2 * tapWrongCount - tapCount;
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreEndless"] == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreEndless"];
            } else if (_score > [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreEndless"] integerValue]) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreEndless"];
            }
        } else {
            int optimal = [KFGameManager optimalTaps];
            // Same but every second counts
            float a = (tapCount - optimal) * 8;
            float b = (timeCount - (optimal)*0.4) * 18;
            
            _score = 1000 - (int)(a + b + tapWrongCount * 20);
            if (_score < 1) {
                _score = 1;
            }
            
            if ([[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreQuick"] == nil) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreQuick"];
            } else if (_score > [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreQuick"] integerValue]) {
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithInt:_score] forKey:@"highScoreQuick"];
            }
        }
        
        // Stop timer
        [timer invalidate];
        
        [self performSegueWithIdentifier:@"gameOver" sender:self];
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view ap_ejectToast:hint.tag];
    [tutorialTimer invalidate];
    tutorialTimer = nil;
    [gameTimer invalidate];
    gameTimer = nil;
    
    if ([[segue identifier] isEqualToString:@"gameOver"]) {
        
        //Check if today is different
        NSDateComponents *components = [[NSCalendar currentCalendar]
                                        components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                        fromDate:[NSDate date]];
        NSDate *startDate = [[NSCalendar currentCalendar]
                             dateFromComponents:components];
        NSDate * pastDate = [[NSUserDefaults standardUserDefaults] objectForKey:@"dayDate"];
        int currented = [[[NSUserDefaults standardUserDefaults] objectForKey:@"dayStreak"] intValue];
        
        if([startDate timeIntervalSinceDate:pastDate] == 24*60*60) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",currented+1] forKey:@"dayStreak"];
            [[NSUserDefaults standardUserDefaults] setObject:startDate forKey:@"dayDate"];
        }
        
        int tNow = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalTaps"] intValue];
        int t2Now = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalTime"] intValue];
        int tgNow = [[[NSUserDefaults standardUserDefaults] objectForKey:@"totalGamesPlayed"] intValue];
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",tNow + tapCount + tapWrongCount] forKey:@"totalTaps"];
        if([KFGameManager gameMode] != KFGameModeEndless)
            [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",t2Now + timeCount] forKey:@"totalTime"];
        
        [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",tgNow + 1] forKey:@"totalGamesPlayed"];
        
        GameOverViewController *vc = [segue destinationViewController];
        vc.score = _score;
        if([KFGameManager gameMode] != KFGameModeTutorial) {
            [KFGameManager sendGameOverStuff:tapCount :timeCount :tapWrongCount :data_master :data_endless : nextRequiredTap:_score];
        }
    }
    
    [super prepareForSegue:segue sender:sender];
}

- (void)selectTile:(KFTile *)tile
{
    
    if (gameState != GameViewTime) {
        tapCount++;
        consecWrong++;
        
        [_label_tapCount setText:[NSString stringWithFormat:@"Taps: %i", tapCount]];
        
        //This has to do when you tapppp
//        KFTile* removedTile = nil;
        if (!tile.hasChildren) {
            if (nextRequiredTap == [tile.value intValue]) {
                if ([KFGameManager gameMode] == KFGameModeTutorial) {
                    tile.enabled = false;
                    tile.isDone = true;
                    tile.transformScale(0.4).spring.makeOpacity(0.1).animateWithCompletion(0.4, JHAnimationCompletion() {
                        tile.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1); //Numbers don't go away now
                    });
                    
                    [self tutorialTapped];
                    
                    
                } else if ([KFGameManager gameMode] == KFGameModeNormal) {
                    tile.enabled = false;
                    tile.isDone = true;
                    tile.transformScale(0.4).spring.makeOpacity(0.1).animateWithCompletion(0.4, JHAnimationCompletion() {
                        tile.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1); //Numbers don't go away now
                    });
                    
                    if (nextRequiredTap == 36) {
                        gameState = GameEnded;
                    }
                    
                } else if ([KFGameManager gameMode] == KFGameModeEndless) {
                    if (nextNums == nil || [nextNums count] <= 0) {
                        nextNums = [KFGameManager genNums];
                        [data_endless addObjectsFromArray:nextNums];
                    }
                    
                    if ((nextNums != nil) && ([nextNums count] > 0)) {
                        tile.enabled = false;
                        tile.transformScale(0.4).spring.makeOpacity(0.1).animateWithCompletion(0.4, JHAnimationCompletion() {
                            [tile setTitle:[nextNums objectAtIndex:0] forState:UIControlStateNormal];
                            tile.value = [nextNums objectAtIndex:0];
                            [nextNums removeObjectAtIndex:0];
                            tile.transform = CGAffineTransformMakeScale(1.0, 1.0);
                            tile.alpha = 1;
                            tile.enabled = true;
                        });
                    }
                    
                    if (nextRequiredTap % 10 == 0) {
                        int toAdd = (int)(pow(10, 140.0 / (nextRequiredTap + 100)) * 1.5);
                        timeCount += toAdd;
                    } else {
                        timeCount += 1;
                    }
                } else {
                    tile.enabled = false;
                    tile.isDone = true;
                    tile.transformScale(0.4).spring.makeOpacity(0.1).animateWithCompletion(0.4, JHAnimationCompletion() {
                        tile.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1); //Numbers don't go away now
                    });
                    
                    if (nextRequiredTap == 16) {
                        gameState = GameEnded;
                    }
                }
                
                current.numViews--;
                nextRequiredTap++;
                consecWrong = 0;
                [KFGameManager playAudio:RDSoundCorrect];
                
            } else {
                tapWrongCount++;
                tapCount--; // Wrong tap doesn't count to total tap count
                
                consecWrong += 10; //(+1 because of global addition)
                if ([KFGameManager gameMode] == KFGameModeEndless)
                    timeCount--;
                else {
                    timeCount++;
                }
                
                tile.enabled = false;
                tile.rotate(20).makeOpacity(0.7).thenAfter(0.1).rotate(-40).thenAfter(0.2).rotate(20).makeOpacity(1.0).animateWithCompletion(0.1, JHAnimationCompletion() {
                    tile.enabled = true;
                });
                
                [KFGameManager playAudio:RDSoundError];
            }
        }
    } else {
        if (!tile.hasChildren) {
            hint.text = @"No selections during the view time";
            [self.view ap_ejectToast:hint.tag];
            hint.tag = [self.view ap_makeToastView:hint duration:1.f position:APToastPositionBottom];
            [KFGameManager playAudio:RDSoundError];
        }
    }
    
    if (tile.hasChildren) {
        if (![tile isEqual:master]) { // Is a clickable children tile
            [KFGameManager playAudio:RDSoundTap];
            if ([KFGameManager gameMode] == KFGameModeTutorial) {
                if (tile == tutorialFolder && tutorialStep == 1) {
                    Pulser *pulse = [[Pulser alloc] init];
                    pulse.parent = [master.children objectAtIndex:(((Pulser *)[tutorialPulses objectAtIndex:1]).parent.tag + 1) % [master.children count]];
                    pulse.oldColor = pulse.parent.backgroundColor;
                    [tutorialPulses addObject:pulse];
                    tutorialFolder = pulse.parent;
                    [self pulseClean:[tutorialPulses objectAtIndex:1]];
                    [tutorialPulses removeObjectAtIndex:1];
                }
//                    return;
            }
            [backButton setAlpha:1];
        } else {
            [backButton setAlpha:0];
        }
        if (tile.tileBoard == current.tileBoard) {
            current = tile;
            return;
        }
        //Current needs to expand....
        tile.backgroundColor = kColorThatBlueColor;
        tile.transformScale(3.0).animateWithCompletion(0.2, JHAnimationCompletion() {
            tile.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
        });
        UIView *tileBoard = current.tileBoard;
        if (tileBoard != nil) {
            tileBoard.tag = 9999;
            if(tile == master) {
                for(KFTile* test in tileBoard.subviews) {
                    if([test isKindOfClass:[KFTile class]]){
                        test.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                        test.transformScale(0.7).animateWithCompletion(0.2, JHAnimationCompletion() {
                            test.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                        });
                    }
                }
            }
            tileBoard.makeOpacity(0).animateWithCompletion(0.2, JHAnimationCompletion() {
                if (tileBoard.tag == 9999) {
                    [tileBoard removeFromSuperview];
                }
            });
        }
        [self.view insertSubview:tile.tileBoard belowSubview:backButton];
        tile.tileBoard.alpha = 0;
        tile.tileBoard.tag = 0;
        tile.tileBoard.transformScale(1.0).makeOpacity(1.0).animate(0.2);
        if(tile != master) {
            for(KFTile* test in tile.tileBoard.subviews) {
                if([test isKindOfClass:[KFTile class]]){
                    test.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.7, 0.7);
                    if(!test.isDone) {
                        test.enabled = true;
                    }
                    test.transformScale(1/0.7).animateWithCompletion(0.2, JHAnimationCompletion() {
                        test.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
                    });
                }
            }
        }
        current = tile;
    }
    
    // Consec wrongs have a buildup, every second you spend looking for the next
    // number it adds up
    if (consecWrong >= 50) {
        hint.text = [NSString stringWithFormat:@"Next Number: %i", nextRequiredTap];
        [self.view ap_ejectToast:hint.tag];
        hint.tag = [self.view ap_makeToastView:hint duration:3.f position:APToastPositionBottom];
        consecWrong = 0;
    }
}

- (void)backButton {
    [KFGameManager playAudio:RDSoundTap];
    if (gameState == GamePlaying) {
        tapCount--; // Back button is freeeeee
    }
    
    // Click back and folder should disapear if there are no objects
    
    if (([KFGameManager gameMode] == KFGameModeNormal ||
         [KFGameManager gameMode] == KFGameModeQuick ||
         [KFGameManager gameMode] == KFGameModeTutorial) &&
        current.numViews == 0) {
        current.alpha = 0.1;
        current.enabled = false;
//        [current removeFromSuperview];
    }
    
    if ([KFGameManager gameMode] == KFGameModeTutorial && gameState != GameViewTime && tutorialFolder == backButton) {
        [self tutorialTapped];
    }
    
    // If user clicks back as free view time ends, all tiles will disappear
    // This makes sure that the current tile isn't master, so you can't exit
    if (!(current == master)) {
        [self selectTile:current.parent];
    }
}

- (CGRect)makeCGRect:(int)v
{
    float div = 3.1;
    float buffer = 50; // Bigger numbers have more space
    return CGRectMake((v % 3) * (maxWidth / div) + (maxWidth / buffer) +
                      (div - floor(div)) * maxWidth / 6,
                      (v / 3) * (maxWidth / div) + (maxWidth / buffer) +
                      ((device == DeviceiPad) ? 40 : 30),
                      (int)((maxWidth / div) - (maxWidth / buffer * 2)),
                      (int)((maxWidth / div) - (maxWidth / buffer * 2)));
}

- (KFTile *)loadStageWithData:(NSObject *)data
{
    KFTile *tile = [[KFTile alloc] init];
    if ([data isKindOfClass:[NSArray class]]) {
        NSArray *stageData = (NSArray *)data;
        NSMutableArray *folder = [[NSMutableArray alloc] init];
        
        UIView *tileBoard = [[UIView alloc] initWithFrame:self.view.bounds];
        for (int v = 0; v < [stageData count]; v++) {
            KFTile *miniTile = [self loadStageWithData:[stageData objectAtIndex:v]];
            [miniTile setFrame:[self makeCGRect:v]];
            if ([stageData count] == 4) {
                miniTile.frame = CGRectMake((v % 2) * (maxWidth / 2) + (maxWidth / 24), (v / 2) * (maxWidth / 2) + (maxWidth / 24) + 30, (maxWidth / 2) - (maxWidth / 12), (maxWidth / 2) - (maxWidth / 12));
            }
            // Has to be squares
            [miniTile addTarget:self action:@selector(makeMePretty:) forControlEvents:UIControlEventTouchDown];
            [miniTile addTarget:self action:@selector(makeMeUgly:) forControlEvents:UIControlEventTouchUpInside];
            [miniTile addTarget:self action:@selector(makeMeUgly:) forControlEvents:UIControlEventTouchUpOutside];
            [miniTile addTarget:self action:@selector(selectTile:) forControlEvents:UIControlEventTouchUpInside];
            [miniTile setUpTile:fontSize * 2];
            
            miniTile.parent = tile;
            miniTile.tag = v;
            
            [folder addObject:miniTile];
            [tileBoard addSubview:miniTile];
        }
        [tileBoard setBackgroundColor:[UIColor clearColor]];
        tile.hasChildren = true;
        tile.children = folder;
        tile.tileBoard = tileBoard;
        [tile setUpTile:fontSize * 2];
    } else {
        tile.hasChildren = false;
        tile.value = (NSString *)data;
    }
    return tile;
}
- (void) makeMePretty : (UIView*) sender {
    sender.backgroundColor = kColorThatBlueColor;
    //Failsafe
    [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(makeMeUgly:) userInfo:sender repeats:NO];
}
- (void) makeMeUgly : (UIView*) sender {
    if([sender isKindOfClass:[NSTimer class]]) {
        sender = [(NSTimer*) sender userInfo];
    }
    sender.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little
 preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

@implementation Pulser

@end
