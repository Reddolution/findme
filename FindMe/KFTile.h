//
//  KFTile.h
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KFTile : UIButton

@property NSMutableArray* children;
@property bool hasChildren;
@property UIView* tileBoard;
@property NSString* value;
@property KFTile* parent;
@property int numViews;
@property bool isDone;

- (void) setUpTile : (float) fontSize;
@end
