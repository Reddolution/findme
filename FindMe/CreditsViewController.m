//
//  CreditsViewController.m
//  FindMe
//
//  Created by Ryan Zhou on 7/14/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "CreditsViewController.h"

@interface CreditsViewController ()

@end

@implementation CreditsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    maxWidth = self.view.bounds.size.width;
    maxHeight = self.view.bounds.size.height;
    if (maxHeight > 800) {
        device = DeviceiPad;
    } else {
        device = DeviceiPhone;
    }
    fontSize = 25.0f;
    if (device == DeviceiPad) {
        fontSize = 40.0f;
    }
    
    menuButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, (device == DeviceiPad) ? 42 : 25)];
    [menuButton setBackgroundColor:[UIColor colorWithRed:0.8 green:0.8 blue:0.8 alpha:1.0]];
    [menuButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [menuButton setTitle:@"Menu" forState:UIControlStateNormal];
    [menuButton.titleLabel setFont:[UIFont fontWithName:@"BebasNeueRegular" size:fontSize]];
    [menuButton addTarget:self action:@selector(menuSelected) forControlEvents:UIControlEventTouchUpInside];
    [menuButton setTitleEdgeInsets:UIEdgeInsetsMake(4.0f, 0.0f, 0.0f, 0.0f)];
    
    [_icons addTarget:self action:@selector(showIconLicense) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:menuButton];
}
-(void)loadView {
    [super loadView];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) menuSelected {
    [self performSegueWithIdentifier:@"creditsToMain" sender:self];
}

-(IBAction)showIconLicense {
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://blog.reddolution.com/Life/?page_id=521"]];
}

- (IBAction) showLicense {
    [Flurry logEvent:@"BEINGHELPFUL_ANDOPENEDLICENCE"];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://blog.reddolution.com/Life/?page_id=247"]];
}
- (IBAction) the24game {
    [self openApp:@"562093274":false];
}
- (void) openApp: (NSString*) appNumber : (bool) isRate {
    [Flurry logEvent:@"BEINGHELPFUL_ANDCLICKEDAPPS"];
    if (NSStringFromClass([SKStoreProductViewController class]) != nil) {
        
        SKStoreProductViewController *storeViewController = [[SKStoreProductViewController alloc] init];
        NSNumber *appId = [NSNumber numberWithInteger:appNumber.integerValue];
        [storeViewController loadProductWithParameters:@{SKStoreProductParameterITunesItemIdentifier:appId} completionBlock:nil];
        storeViewController.delegate = self;
        [self presentViewController:storeViewController animated:YES completion:^{
            //Temporarily use a black status bar to match the StoreKit view.
            [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
        }];
        
        //Use the standard openUrl method if StoreKit is unavailable.
    } else {
        NSLog(@"URL Directing");
        if(isRate)
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%li",(long)appNumber.integerValue]]];
        else
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"itms-apps://itunes.apple.com/us/app/the-24-game/id%li?mt=8&uo=4",(long)appNumber.integerValue]]];
    }
}

- (IBAction) suggestion {
    MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
    picker.mailComposeDelegate = self;
    
    [picker setSubject:@"0005O3:Suggestions/Questions"];
    
    // Set up the recipients.
    NSArray *toRecipients = [NSArray arrayWithObjects:@"reddhelp@gmail.com",
                             nil];
    
    [picker setToRecipients:toRecipients];
    
    // Fill out the email body text.
    
    // Present the mail composition interface.
    [self presentViewController:picker animated:YES completion:nil];
}

// The mail compose view controller delegate method
- (void)mailComposeController:(MFMailComposeViewController *)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    // get the top most controller (= the StoreKit Controller) and dismiss it
    UIViewController *presentingController = self;
    [presentingController dismissViewControllerAnimated:YES completion:^{
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
