//
//  DataSender.h
//  N24
//
//  Created by Kevin Fang on 7/24/13.
//  Copyright (c) 2013 Kevin Fang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GameKit/GameKit.h>
#import "NSData+Base64.h"
#import "NSDataEncryption.h"

@interface DataSender : NSObject

+ (void) EncryptAndSubmitScore: (NSDictionary*) data;
+ (void) SubmitScore: (NSData*) data;
+ (void) SubmitOldScores;
+ (NSDictionary*) leaderboard_load : (NSString*) params;
+ (NSString*) request_id;
+ (bool) request_challenge;
+ (NSMutableArray*) get_challenge;
+ (void) send_alias;
+ (bool) connection_test;


+ (NSMutableDictionary*) loadSettings;

+ (void) saveData;



+ (void) dictSetObject: (NSString*) object forKey: (NSString*) key;
+ (id) dictObjectForKey: (NSString*) key;
@end
