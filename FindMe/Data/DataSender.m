//
//  DataSender.m
//  N24
//
//  Created by Kevin Fang on 7/24/13.
//  Copyright (c) 2013 Kevin Fang. All rights reserved.
//

#import "DataSender.h"
#import <CommonCrypto/CommonDigest.h>

@implementation DataSender

static NSString *bunnies = @"12345678901234567890123456789012";
static NSMutableDictionary* settings = nil;
+ (void) SubmitOldScores {
    NSLog(@"Submitting Old Scores");
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        if([[[DataSender loadSettings] objectForKey:@"Alias"] length] > 0)
            [DataSender send_alias];
        NSMutableDictionary* dict = [DataSender loadSettings];
        long total = [[dict objectForKey:@"Scores2Push"] count];
        NSLog(@"Looks like there is %li old scores",total);
        while(total > 0) {
            [DataSender SubmitScore:[[dict objectForKey:@"Scores2Push"] objectAtIndex:0]];
            [[dict objectForKey:@"Scores2Push"] removeObjectAtIndex:0];
            [self saveData];
            total--;
        }
    });
}
+ (void) EncryptAndSubmitScore:(NSDictionary *)dict {
    NSMutableData *data = [[NSMutableData alloc] init];
    NSKeyedArchiver *archiver = [[NSKeyedArchiver alloc] initForWritingWithMutableData:data];
    [archiver encodeObject:dict];
    [archiver finishEncoding];
    
    [DataSender SubmitScore: (NSData*) data];
}
+ (void) SubmitScore:(NSData *)data {
    NSMutableDictionary* dict = [DataSender loadSettings];
    NSKeyedUnarchiver *unarchiver = [[NSKeyedUnarchiver alloc] initForReadingWithData:data];
    NSDictionary *myDictionary = [unarchiver decodeObject];
    [unarchiver finishDecoding];
    if([DataSender server_sendRequest: myDictionary : [NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/score_submit.py"]]) {
        NSLog(@"SUCCESSFUL submit - %@",[DataSender md5:data]);
    } else {
        NSLog(@"ERROR submit - %@",[DataSender md5:data]);
        
        [[dict objectForKey:@"Scores2Push"] addObject:data];
        if([[dict objectForKey:@"Scores2Push"] count] > 100) {
            [[dict objectForKey:@"Scores2Push"] removeObjectAtIndex:0];
        }
        [self saveData];
    }
}

+ (NSString*)md5 : (NSData*) data
{
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5( data.bytes, (CC_LONG) data.length, result ); // This is the md5 call
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (bool) server_sendRequest: (NSDictionary*) keys : (NSURL*) url {
    NSString* content = [self server_sentRequest:keys :url];
    if(content == nil || [content length] == 0 || [content rangeOfString:@"DONE"].location == NSNotFound){// || [content rangeOfString:@"ERROR"].location != NSNotFound) {
        return false;
    } else {
        return true;
    }
}
+ (NSString*) server_sentRequest: (NSDictionary*) keys : (NSURL*) url {
    NSData* returnData = [DataSender server_request:keys :url];
    if(returnData == nil) {
        return [NSString stringWithFormat:@"%i",arc4random() % 5000 + 10000];
    }
    NSString *content = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    return content;
}
+ (NSData*) server_request: (NSDictionary*) keys : (NSURL*) url {
    if(![DataSender connection_test]) {
        return nil;
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"POST"];
    
    NSData *newProjectJSONData = [NSJSONSerialization dataWithJSONObject:keys options:NSJSONWritingPrettyPrinted error:nil];
    NSString* b = [[newProjectJSONData AES256EncryptWithKey:bunnies] base64EncodedString];
    NSString* noEqual = [b stringByReplacingOccurrencesOfString:@"=" withString:@"%3D"];
    noEqual = [noEqual stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
    [request setHTTPBody:[[NSString stringWithFormat:@"data=%@",noEqual]dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:NO]];
    
    
    NSURLResponse *response;
    NSError *err;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    return returnData;
}
+ (NSDictionary *)leaderboard_load:(NSString *)params {
    NSLog(@"Loading Leaderboard");
    if(![DataSender connection_test]) {
        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"ERROR",@"Try Refreshing",@":(", nil] forKeys:[NSArray arrayWithObjects:@"score",@"name",@"place", nil]]] forKeys:[NSArray arrayWithObject:@"0"]];
    }
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://apps.reddolution.com/Apps/0004O3/V1/score_show.py?%@",params]]];
    
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    
    NSURLResponse *response;
    NSError *err;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    
    if(returnData == nil) {
        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"ERROR",@"Try Refreshing",@":(", nil] forKeys:[NSArray arrayWithObjects:@"score",@"name",@"place", nil]]] forKeys:[NSArray arrayWithObject:@"0"]];
    }
    NSDictionary* ret =[NSJSONSerialization JSONObjectWithData:returnData options:0 error:nil];
    if(ret == nil) {
        return [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"ERROR",@"Try Refreshing",@":(", nil] forKeys:[NSArray arrayWithObjects:@"score",@"name",@"place", nil]]] forKeys:[NSArray arrayWithObject:@"0"]];
    }
    return ret;
    
    
}
+ (bool) connection_test {
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/connection_test.py"]];
    [request addValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPMethod:@"GET"];
    [request setTimeoutInterval:2.0];
    NSURLResponse *response;
    NSError *err;
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
    NSString *content = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
    
    if([content rangeOfString:@"SHIPSDOCKED"].location == NSNotFound) { // || [content rangeOfString:@"ERROR"].location != NSNotFound) {
        return false;
    } else {
        return true;
    }
}

+ (NSMutableArray *)get_challenge {
    NSLog(@"Retrieving challenge");
    NSDictionary* myDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString]] forKeys:[NSArray arrayWithObject:@"userid"]];
    NSString* ret = [DataSender server_sentRequest:myDictionary :[NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/challenge_download.py"]];
    if([ret rangeOfString:@"KEVINISAWESOME"].location == NSNotFound) {
        return nil;
    }
    ret = [[ret stringByReplacingOccurrencesOfString:@"KEVINISAWESOME" withString:@""] stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"\n"]];
    //    NSLog(@"%@",wtf);
    NSError * err;
    NSDictionary* dis = [NSJSONSerialization JSONObjectWithData:[[NSData dataFromBase64String:ret] AES256DecryptWithKey:bunnies] options:0 error:&err];
    NSDictionary* data;
    NSMutableArray* array = [[NSMutableArray alloc] init];
    int v=0;
    while((data = [dis objectForKey:[NSString stringWithFormat:@"%i",v]]) != nil) {
        [array addObject:data];
        v++;
    }
    return array;
}
+ (bool)request_challenge {
    NSLog(@"Asking if can do challenge");
    NSDictionary* myDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString]] forKeys:[NSArray arrayWithObject:@"userid"]];
    return [DataSender server_sendRequest: myDictionary : [NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/challenge_request.py"]];
}
+ (NSString *)request_id {
    NSLog(@"Requesting ID");
    NSDictionary* myDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObject:[[[UIDevice currentDevice] identifierForVendor] UUIDString]] forKeys:[NSArray arrayWithObject:@"userid"]];
    return [[DataSender server_sentRequest: myDictionary : [NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/alias_request.py"]] stringByReplacingOccurrencesOfString:@"USER SUCCESFFUL" withString:@""];
    
}
+ (void)send_alias {
    NSLog(@"Submiting ALIAS");
    NSDictionary* myDictionary = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:[[[UIDevice currentDevice] identifierForVendor] UUIDString],[[DataSender loadSettings] objectForKey:@"Alias"],nil] forKeys:[NSArray arrayWithObjects:@"userid",@"alias",nil]];
    [DataSender server_sendRequest: myDictionary : [NSURL URLWithString:@"http://apps.reddolution.com/Apps/0004O3/V1/alias_submit.py"]];
}

+ (NSMutableDictionary*) loadSettings {
    if(settings != nil) {
        return settings;
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Settings.plist"];
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        // if not in documents, get property list from main bundle
        plistPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
    }
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSError *errorDesc = nil;
    NSPropertyListFormat format;
    NSMutableDictionary* dict =  (NSMutableDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
    if([dict objectForKey:@"Version"] == nil) {
        [dict setObject:[NSString stringWithFormat:@"%i", version] forKey:@"Version"];
        [self saveData];
    }
    settings = dict;
    int v = [[dict objectForKey:@"Version"] intValue];
    if(v == version) {
        NSLog(@"Same Version v%i",version);
    } else if(v == 12) {
        [dict setObject:[NSString stringWithFormat:@"%i",version] forKey:@"Version"];
        [self saveData];
        [[[UIAlertView alloc] initWithTitle:@"New Update" message:@"Hello Everybody!\n Name Change! MADS 24\n\n If you like the game,\n help us spread the word\n and improve everyone's mental abilities!\n\nIf you don't,\n contact us and tell us how to make it better!\n\n And don't forget to have fun!" delegate:nil cancelButtonTitle:@"Got it!" otherButtonTitles:nil] show];
    } else if(v < version) {
        NSLog(@"Mismatch version number %@ vs app requirement %i",[dict objectForKey:@"Version"],version);
        NSLog(@"ALSO NO CATCH");
        plistPath = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"plist"];
        plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
        dict =  (NSMutableDictionary *)[NSPropertyListSerialization propertyListWithData:plistXML options:NSPropertyListMutableContainersAndLeaves format:&format error:&errorDesc];
    } else {
        NSLog(@"WTF, WHY AM I HERE?");
    }
    settings = dict;
    return dict;
    
}
+ (void) dictSetObject: (NSString*) object forKey: (NSString*) key {
    [[DataSender loadSettings] setObject:object forKey:key];
    [DataSender saveData];
}

+ (id) dictObjectForKey: (NSString*) key {
    return [[DataSender loadSettings] objectForKey:key];
}

+ (void) saveData
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Settings.plist"];
    
    NSString *error = nil;
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:settings format:NSPropertyListXMLFormat_v1_0 errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
    }
    else {
        NSLog(@"Error in saveData: %@", error);
    }
}

+ (NSData*) encryptString:(NSString*)plaintext withKey:(NSString*)key {
    return [[plaintext dataUsingEncoding:NSUTF8StringEncoding] AES256EncryptWithKey:key];
    
}

+ (NSString*) decryptData:(NSData*)ciphertext withKey:(NSString*)key {
    return [[NSString alloc] initWithData:[ciphertext AES256DecryptWithKey:key]
                                 encoding:NSUTF8StringEncoding];
}
@end
