//
//  GameOverViewController.h
//  FindMe
//
//  Created by Ryan Zhou on 7/7/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>


@interface GameOverViewController : UIViewController <ADBannerViewDelegate,GKGameCenterControllerDelegate>



@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *highScoreLabel;
@property int score;
- (IBAction)shareStuff:(id)sender;

@end
