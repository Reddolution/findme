//
//  CreditsViewController.h
//  FindMe
//
//  Created by Ryan Zhou on 7/14/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>
#import <MessageUI/MessageUI.h>
#import "KFFlatGameButton.h"

typedef enum : NSUInteger {
    DeviceiPad,
    DeviceiPhone,
} Device;

@interface CreditsViewController : UIViewController <SKStoreProductViewControllerDelegate,MFMailComposeViewControllerDelegate> {
    UIButton* menuButton;
    Device device;
    float maxHeight;
    float maxWidth;
    float fontSize;
}

@property (weak, nonatomic) IBOutlet KFFlatGameButton *license;
@property (weak, nonatomic) IBOutlet KFFlatGameButton *icons;

@end
