//
//  ViewController.h
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFFlatGameButton.h"
#import <GameKit/GameKit.h>

@interface ViewController : UIViewController <GKGameCenterControllerDelegate,CNPPopupControllerDelegate,AppodealBannerDelegate> {
    UILabel* dayStreak;
}

@property (weak, nonatomic) IBOutlet KFFlatGameButton *credits;



@end

