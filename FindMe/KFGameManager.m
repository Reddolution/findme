    //
//  KFGameManager.m
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "KFGameManager.h"

@implementation KFGameManager

+ (KFGameManager *)sharedInstance {
    if(shared == nil) {
        shared = [[KFGameManager alloc] init];
        shared->currEndless = 0;
        
        
        NSMutableArray* sounds = [[NSMutableArray alloc] init];
        NSArray* a = [[NSArray alloc] initWithObjects:@"Tap",@"Correct",@"Error",@"StartGame", nil];
        for(NSString* fileName in a) {
            NSURL *tapSound   = [[NSBundle mainBundle] URLForResource: [NSString stringWithFormat:@"Sounds/%@",fileName] withExtension: @"mp3"];
            SystemSoundID soundFile;
            AudioServicesCreateSystemSoundID ((__bridge CFURLRef) tapSound, &soundFile);
            [sounds addObject:[NSString stringWithFormat:@"%u",(unsigned int)soundFile]];
        }
        shared->gameSounds = sounds;
        
    }
    return shared;
}

+ (NSArray *) setUpGame {
    
    KFGameMode stage = shared->gameMode;
    
    NSMutableArray* master = [[NSMutableArray alloc] init];
    
    
    if(stage == KFGameModeNormal || stage == KFGameModeEndless || stage == KFGameModeTutorial) {
        //9*9 = 81 numbers (1-81)
        //Folders with numbers
        NSMutableSet* set = [[NSMutableSet alloc] init];
        for(int v=0;v<9;v++) {
            NSMutableArray* folder = [[NSMutableArray alloc] init];
            for(int f=0;f<4;f++) {
                //                int randomNumber = v*4 + f + 1;
                int randomNumber = arc4random() % 36 + 1;
                NSString* RandomNumber = [NSString stringWithFormat:@"%i",randomNumber];
                if(![set containsObject:RandomNumber]) {
                    [set addObject:RandomNumber];
                    [folder addObject:RandomNumber];
                } else {
                    f--;
                }
            }
            [master addObject:folder];
        }
    } else if (stage == KFGameModeQuick) {
        //16 numbers?
        NSMutableSet* used = [[NSMutableSet alloc] init];
        
        for (int i = 0; i < 4; i++) {
            NSMutableArray* folder = [[NSMutableArray alloc] init];
            
            for (int j = 0; j < 4; j++) {
                int randomNumber = arc4random() % 16 + 1;
                
                NSString* rand = [NSString stringWithFormat:@"%i", randomNumber];
                
                if (![used containsObject:rand]) {
                    [folder addObject:rand];
                    [used addObject:rand];
                } else {
                    j--;
                }
            }
            [master addObject:folder];
        }
    }
    
    if (stage == KFGameModeEndless) {
        [self sharedInstance]->currEndless = 1;
        
    }
    
    shared.master = master;
    
    //stage 2 is endless
    return master;
}
+ (int) optimalTaps {
    NSArray* master = shared.master;
    int optimal = 0;
    for(int v=0;v<[master count];v++) {
        optimal += 2*[[master objectAtIndex:v] count];
        for(NSString* num in [master objectAtIndex:v]) {
            if([[master objectAtIndex:v] containsObject:[NSString stringWithFormat:@"%i",[num intValue] + 1]]) {
                optimal--;
            }
        }
    }
    return optimal;
}

+ (NSMutableArray*) genNums {
    NSMutableArray* toReturn = [[NSMutableArray alloc] init];
    NSMutableSet* used = [[NSMutableSet alloc] init];
    
    for (int i = 0; i < 36; i++) {
        int randomNumber = arc4random() % 36 + [self sharedInstance]->currEndless * 36 + 1;
        //int randomNumber = [self sharedInstance]->currEndless * 36 + 1 + i;
        NSString* rand = [NSString stringWithFormat:@"%i", randomNumber];
        
        if (![used containsObject:rand]) {
            [toReturn addObject:rand];
            [used addObject:rand];
        } else {
            i--;
        }
    }
    
    shared->currEndless++;
    
    return toReturn;
}

+ (void)playAudio:(RDSound)sound {
    if(![[DataSender dictObjectForKey:@"SFX"] boolValue])
        return;
        
    AudioServicesPlaySystemSound((SystemSoundID)[[shared->gameSounds objectAtIndex:sound] longLongValue]);
}

+ (KFGameMode)gameMode {
    return shared->gameMode;
}

+ (void)setGameMode:(KFGameMode)gameMode {
    shared->gameMode = gameMode;
}

+ (void)sendGameOverStuff:(int)taps :(int)time :(int)wrongTaps : (NSMutableArray*) master : (NSMutableArray*) endless :(int)nextCount :(int)score{
    NSDate *currentTime = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitHour fromDate:currentTime];
    
    NSMutableString* masterString = [[NSMutableString alloc] init];

    for (NSMutableArray* m in master) {
        for(NSString* i in m) {
            //Padding!!!
            if([i length] == 3) {
                //DO NOTHING
                [masterString appendString:i];
            } else if([i length] == 2){
                [masterString appendString:@"0"];
                [masterString appendString:i];
            } else if([i length] == 1){
                [masterString appendString:@"00"];
                [masterString appendString:i];
            }
        }
    }
    if(shared->gameMode != KFGameModeEndless) {
        endless = nil;
    }
    if(endless != nil) {
        for(NSString* i in endless) {
            if([i length] == 3) {
                [masterString appendString:i];
            } else if([i length] == 2){
                [masterString appendString:@"0"];
                [masterString appendString:i];
            } else if([i length] == 1){
                [masterString appendString:@"00"];
                [masterString appendString:i];
            }
        }
    }
    
    
    NSDictionary* data = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:
                                                      [[[UIDevice currentDevice] identifierForVendor] UUIDString],
                                                      [NSString stringWithFormat:@"%i",(int)shared->gameMode],
                                                      [NSString stringWithFormat:@"%i",score],
                                                      [NSString stringWithFormat:@"%i",[KFGameManager optimalTaps]],
                                                      [NSString stringWithFormat:@"%i",(endless==nil?time:nextCount)],
                                                      [NSString stringWithFormat:@"%i",taps],
                                                      [NSString stringWithFormat:@"%i",wrongTaps],
                                                      masterString,
                                                      [NSString stringWithFormat:@"%li",(long)[components hour]],
                                                      [dateFormatter stringFromDate: currentTime],
                                                      [NSString stringWithFormat:@"%@v%@-%@",[[UIDevice currentDevice] model],[[UIDevice currentDevice] systemVersion],[[UIDevice currentDevice] systemName]],
                                                      [NSString stringWithFormat:@"%@",[[GKLocalPlayer localPlayer] playerID]==nil?@"":[[GKLocalPlayer localPlayer] playerID]],
                                                      [NSString stringWithFormat:@"%i",version],
                                                      nil]
                                             forKeys:[NSArray arrayWithObjects:@"user_id",@"game_type",@"game_score",@"game_optimal",@"game_time",@"game_taps",@"game_wrongTaps",@"data",@"device_hour",@"device_dateofplay",@"device_type",@"user_gamecenterid",@"version", nil]];
    
    [DataSender EncryptAndSubmitScore:data];
}

@end
