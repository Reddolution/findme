//
//  KFFlatGameButton.m
//  FindMe
//
//  Created by Kevin Fang on 7/1/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "KFFlatGameButton.h"

@implementation KFFlatGameButton

/*
 Only override drawRect: if you perform custom drawing.
 An empty implementation adversely affects performance during animation.

*/


- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    
    [self setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if ([self backgroundImageForState:UIControlStateNormal] == nil)
        self.backgroundColor = [UIColor colorWithRed:0.2 green:0.4 blue:0.8 alpha:1.0];
    self.layer.cornerRadius = 8.0;
    if ( [(NSString*)[UIDevice currentDevice].model hasPrefix:@"iPad"] ) {
        self.layer.cornerRadius = 15.0;
    }
    [self setTitleEdgeInsets:UIEdgeInsetsMake(10.0f, 0.0f, 0.0f, 0.0f)];
    
    return self;
}
@end
