//
//  GameOverViewController.m
//  FindMe
//
//  Created by Ryan Zhou on 7/7/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "GameOverViewController.h"
#import <GameKit/GameKit.h>

@interface GameOverViewController () {
    NSString* value;
}

@end

@implementation GameOverViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [_scoreLabel setText:[NSString stringWithFormat:@"Score: %i", _score]];
    int highScore;
    if ([KFGameManager gameMode] == KFGameModeNormal) {
        highScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreNormal"] integerValue];
        value = @"normal";
    } else if ([KFGameManager gameMode] == KFGameModeEndless) {
        highScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreEndless"] integerValue];
        value = @"endless";
    } else {
        highScore = [[[NSUserDefaults standardUserDefaults] objectForKey:@"highScoreQuick"] integerValue];
        value = @"quick";
    }
    
    [_highScoreLabel setText:[NSString stringWithFormat:@"High Score: %i", highScore]];
    //This is where score is submitted;
    
    if ([GKLocalPlayer localPlayer].isAuthenticated) {
        dispatch_after(2.0, dispatch_get_main_queue(), ^{
            if ([GKLocalPlayer localPlayer].isAuthenticated) {
                GKScore* score = [[GKScore alloc] initWithLeaderboardIdentifier:[NSString stringWithFormat:@"findme.%@",value]];
                score.value = _score;
                [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
                    if (error) {
                        // handle error
                        NSLog(@"ERRRRRRROR");
                    }
                }];
            } else {
                NSLog(@"Was not authenticated");
            }
        });
    }
    
    UIView* notthis = self.view;
    if(self.view.frame.size.width > 700) {
        self.view.frame = CGRectMake(768/4, 1024/4, 768/2, 1024/2);
        self.view.transform = CGAffineTransformMakeScale(2, 2);
        //        self.view.frame = CGRectMake(0, 0, 768, 1024);
        NSLog(@"%f",[[UIScreen mainScreen] bounds].size.width);
        self.view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
        [self.view addSubview:notthis];
//        ADBannerView *adView = [[ADBannerView alloc] initWithFrame: CGRectMake(0, 1024-66, 768, 66)];
//        adView.delegate = self;
//        [self.view addSubview: adView];
//        adView.alpha = 0;
    } else {
//        ADBannerView *adView = [[ADBannerView alloc] initWithFrame: CGRectMake(0, self.view.frame.size.height-50,self.view.frame.size.width,50)];
//        adView.delegate = self;
//        [self.view addSubview: adView];
//        adView.alpha = 0;
    }
}
bool hasSeen = false;
- (void)viewWillAppear:(BOOL)animated {
    if(!hasSeen) {
        if(arc4random() % 5 == 1) {
            [Appodeal showAd:AppodealShowStyleSkippableVideo rootViewController:self];
            
        } else if(arc4random() % 5 == 1) {
            
        } else {
            [Appodeal showAd:AppodealShowStyleInterstitial rootViewController:self];
        }
        hasSeen = true;
    }
}
-(void)viewDidAppear:(BOOL)animated {
    
}
- (void)loadView {
    [super loadView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bannerView:(ADBannerView*)banner didFailToReceiveAdWithError:(NSError*)error {
    NSLog(@"bannerview did not receive any banner due to %@", error);
    banner.alpha = 0;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner{
    NSLog(@"bannerview was selected");
    
}

-(BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    // this indicates that the banner has been clicked.. confirm?
    NSLog(@"banner clicked: will %s application\n",willLeave?"leave":"cover");
    
    return YES;
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner {
    NSLog(@"banner was loaded");
    banner.alpha = 1;
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)stuff:(id)sender {
    NSLog(@"HAPP");
}


- (IBAction)openGameCenter:(id)sender {
    
        GKGameCenterViewController* leaderboardController = [[GKGameCenterViewController alloc] init];
        
        if (leaderboardController != NULL)
        {
            leaderboardController.viewState = GKGameCenterViewControllerStateLeaderboards;
            
            leaderboardController.leaderboardIdentifier = [NSString stringWithFormat:@"findme.%@",value];
            leaderboardController.gameCenterDelegate = self;
            [self presentViewController:leaderboardController animated:YES completion:nil];
        }
//    }
    
}

#pragma mark gamecenter
-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController {
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)shareStuff:(id)sender {
    NSString* whatTodo = @"Normal";
    switch ([KFGameManager gameMode]) {
        case KFGameModeEndless:
            whatTodo = @"Endless";
            break;
        case KFGameModeNormal:
            whatTodo = @"Normal";
            break;
        case KFGameModeQuick:
            whatTodo = @"Quick";
            break;
        default:
            break;
    }
    NSString *texttoshare = [NSString stringWithFormat:@"I got %i in %@ on One236, challenge me now! https://itunes.apple.com/app/id1018549465",_score,whatTodo];
    NSArray *activityItems = @[texttoshare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];

    activityVC.excludedActivityTypes = @[UIActivityTypeAssignToContact, UIActivityTypePrint, UIActivityTypePostToTwitter, UIActivityTypePostToWeibo];
    [self presentViewController:activityVC animated:TRUE completion:nil];
}
@end
