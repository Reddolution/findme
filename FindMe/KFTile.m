//
//  KFTile.m
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import "KFTile.h"

@implementation KFTile

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (void)setUpTile : (float) fontSize {
    [self setTitle:self.value forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor colorWithWhite:0.9 alpha:1.0]];
    [self setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont fontWithName:@"BebasNeueRegular" size:fontSize]];
    self.layer.cornerRadius = 15.0;
    self.layer.borderWidth = 2.0f;
    self.layer.borderColor = [UIColor colorWithRed:0.369 green:0.366 blue:0.359 alpha:1.000].CGColor;
    [self setTitleEdgeInsets:UIEdgeInsetsMake(6.0f, 0.0f, 0.0f, 0.0f)];
    
    _numViews = (int) [_children count];
}
@end
