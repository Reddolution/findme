//
//  GameViewController.h
//  FindMe
//
//  Created by Kevin Fang on 6/25/15.
//  Copyright (c) 2015 Kevin Fang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KFGameManager.h"
#import "CircleProgressBar.h"
#import "KFTile.h"
#import <APToast/UIView+APToast.h>

typedef enum : NSUInteger {
    GamePrepare = 1,
    GamePaused = 2,
    GameViewTime = 4,
    GamePlaying = 8,
    GameEnded = 16,
} GameStatus;

typedef enum : NSUInteger {
    DeviceiPad,
    DeviceiPhone,
} Device;

@interface GameViewController : UIViewController <CNPPopupControllerDelegate> {
    float maxHeight;
    float maxWidth;
    Device device;
    float fontSize;
    
    
    NSTimer* gameTimer;
    NSArray* data_master;
    NSMutableArray* data_endless;
    KFTile* master;
    int tapCount;
    int tapWrongCount;
    int timeCount;
    int nextRequiredTap;
    KFTile* current;
    KFTile* backButton;
    GameStatus gameState;
    CircleProgressBar* barTime;
    NSMutableArray* nextNums;
    int consecWrong;
    
    
    UIButton* menuButton;
    UIButton* menuButtonCancel;
    UILabel* hint;
    
    CNPPopupController* tutorialController;
    int tutorialStep;
    KFTile* tutorialFolder;
    UILabel* tutorialText;
    NSMutableArray* tutorialPulses;
    NSTimer* tutorialTimer;
    
    AVAudioPlayer* background;
}

@property int score;
@property (weak, nonatomic) IBOutlet UILabel *label_tapCount;
@property (weak, nonatomic) IBOutlet UILabel *label_timeCount;

- (void) loadStage;



@end
@interface Pulser : NSObject

@property KFTile* parent;
@property UIColor* oldColor;

@end
